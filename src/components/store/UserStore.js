import { extendObservable } from "mobx";

import React from "react";
extendObservable(this, {
  loading: true,
  isLoggedIn: false,
  username: "",
});

const UserStore = () => {
  return <div></div>;
};

export default new  UserStore();
