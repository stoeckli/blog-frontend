import { useState, useEffect } from "react";
import PersonalBlog from "../PersonalBlog/PersonalBlog.js";
import styledPersonalBlogEntries from "./PersonalBlogEntries.css";
import axios from "axios";
import Cookies from "js-cookie";

const PersonalBlogEntries = () => {
  const [blogs, setBlogs] = useState([]);

  const [username, setUsername] = useState("");

  useEffect(() => {
    setUsername(Cookies.get("username"));
  }, []);

  useEffect(() => {
    try {
      const url = `https://ibaw-blog-backend.herokuapp.com/userAccount/userId`;
      const userCredentials = {
        username: Cookies.get("username"),
      };

      axios.post(url, userCredentials).then((res) => {
        if (res.data.success) {
          getPersonalBlogs(res.data.userId);
        }
      });
    } catch (err) {
      console.log(err);
    }
  }, [username]);

  const getPersonalBlogs = (userId) => {
    try {
      const url = `https://ibaw-blog-backend.herokuapp.com/myblogs/${userId}`;
      axios
        .get(url)
        .then((res) => {
          setBlogs(res.data.blogs);
        })
        .catch();
    } catch (err) {
      console.log(err);
    }
  };

  const clickToBlog = (currentBlog) => {
    setBlogs(
      blogs.filter((blog) => {
        return blog._id == currentBlog._id;
      })
    );
  };
  return (
    <>
      {blogs.length > 0 ? (
        <section className="personal-grid-blogs">
          {blogs.map((blog) => (
            <PersonalBlog
              key={blog._id}
              blogs={blogs}
              blog={blog}
              onClick={clickToBlog}
            />
          ))}
        </section>
      ) : (
        <section>
          <h1 className="noBlogs">-- You have no blog entries --</h1>
        </section>
      )}
    </>
  );
};

export default PersonalBlogEntries;
