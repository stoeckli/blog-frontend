import { useState, useEffect } from "react";
import axios from "axios";
import styledSocket from "./Socket.css";
import io from "socket.io-client";
import { Link } from "react-router-dom";
import Cookies from "js-cookie";

const socket = io.connect("https://ibaw-blog-backend.herokuapp.com/");

const Socket = ({ createdBlogId }) => {
  const [test, setTest] = useState("");
  const [createdBlog, setCreatedBlog] = useState([]);

  useEffect(() => {
    if (createdBlogId !== "") {
      const url = `https://ibaw-blog-backend.herokuapp.com/blog/${createdBlogId}`;
      const token = Cookies.get("token");
      axios
        .get(url, { headers: { Authorization: token } })
        .then((res) => {
          if (res.data.success) {
            setCreatedBlog(res.data.blog);
          }
        })
        .catch();
    }
  }, []);

  const hideForm = () => {
    setTest("hidden");
  };

  return (
    <>
      {Object.keys(createdBlog).length !== 0 ? (
        <form className={test}>
          <div id="popup1" className="overlay">
            <div className="popup">
              <h3>New Blog!</h3>
              <div className="close" onClick={hideForm}>
                &times;
              </div>
              <div className="information-text">
                A new blog article has been created. Check it out if you are
                interested:
                <br />
                <Link
                  onClick={hideForm}
                  to={{
                    pathname: "/BlogEntry",
                    state: {
                      blog: createdBlog,
                    },
                  }}
                  blog={"test"}
                >
                  New Blog Entry
                </Link>
              </div>
            </div>
          </div>
        </form>
      ) : (
        ""
      )}
    </>
  );
};

export default Socket;
