import React, { useState, useEffect } from "react";
import ReactTooltip from "react-tooltip";
import ToggleButton from "react-toggle-button";
import styledProfile from "./Profile.css";
import ProfileValidation from "../Validation/ProfilValidation";
import axios from "axios";
import ButtonChangePassword from "../ButtonChangePassword/ButtonChangePassword.js";
import { useHistory } from "react-router-dom";
import Cookies from "js-cookie";
import UploadProfileImage from "../UploadProfileImage/UploadProfileImage.js";

const Profile = ({ setUserLogged, userImageUrl }) => {
  const [message, setMessage] = useState("");
  const [token, setToken] = useState("");
  const [username, setUsername] = useState("");
  const [changePassword, setChangePassword] = useState(false);
  const [password, setPassword] = useState("");
  const [email, setEmail] = useState("cedric.stoeckli@gmail.com");
  const [confirmedPassword, setConfirmedPassword] = useState("");
  const [oldPassword, setOldPassword] = useState("");
  const [errors, setErrors] = useState({});
  const [uploadedURL, setUploadedURL] = useState(
    "https://firebasestorage.googleapis.com/v0/b/blog-firebase-87efd.appspot.com/o/DefaultImages%2Fwomen.jpg?alt=media&token=5f7930f9-b772-4c83-8b4e-23d6ccdd668a"
  );
  const [profileActivated, setProfileActivated] = useState(true);

  const history = useHistory();

  const saveCredentials = (e) => {
    e.preventDefault();
    setErrors(ProfileValidation({ username }));
    if (changePassword) {
      setErrors(
        ProfileValidation({
          username,
          email,
          oldPassword,
          password,
          confirmedPassword,
        })
      );
      if (
        Object.keys(
          ProfileValidation({
            username,
            oldPassword,
            password,
            confirmedPassword,
          })
        ).length != 0
      ) {
        return;
      }
    } else {
      setErrors(ProfileValidation({ username }));
    }
    if (!username) {
      alert("Please add a username");
      return;
    }
    getUserId(username);
  };

  const getUserId = () => {
    try {
      const url = `https://ibaw-blog-backend.herokuapp.com/userAccount/userId`;
      const userCredentials = {
        username: Cookies.get("username"),
      };

      axios.post(url, userCredentials).then((res) => {
        if (res.data.success) {
          if (
            uploadedURL !==
            "https://firebasestorage.googleapis.com/v0/b/blog-firebase-87efd.appspot.com/o/DefaultImages%2Fwomen.jpg?alt=media&token=5f7930f9-b772-4c83-8b4e-23d6ccdd668a"
          ) {
            updateUser(res.data.userId);
          }
          changeUserInformation(res.data.userId);
        }
        if (!profileActivated) {
          deleteUser(res.data.userId);
        }
      });
    } catch (err) {
      console.log(err);
    }
  };

  const updateUser = (userId) => {
    try {
      const url = `https://ibaw-blog-backend.herokuapp.com/userAccount/${userId}`;

      const updateInformation = {
        imageUrl: uploadedURL,
      };
      const token_ = Cookies.get("token");
      axios
        .put(url, updateInformation, { headers: { Authorization: token_ } })
        .then((res) => {
          if (res.data.success) {
            // setMessage(res.data.message);
            // const url =
            //   `http://localhost:3001/userAccount/logout?token=` + token.token;
            // axios.get(url).then((res) => {
            //   if (res.data.success) {
            //     Cookies.remove("token");
            //     Cookies.remove("username");
            //     setUserLogged(false);
            //     history.push("/success");
            //   }
            // });
          }
        });
    } catch (err) {
      console.log(err);
    }
  };

  const changeUserInformation = (userId) => {
    try {
      updateUserInformation(
        userId,
        username,
        oldPassword,
        password,
        confirmedPassword
      );
    } catch (err) {
      console.log(err);
    }
  };

  const updateUserInformation = (
    userId,
    username,
    oldPassword,
    password,
    confirmedPassword
  ) => {
    try {
      const url = `https://ibaw-blog-backend.herokuapp.com/userAccount/changeUserCredentials/${userId}`;

      const UserCredentials = {
        username: username,
        changePassword: false,
        imageUrl: uploadedURL,
      };

      if (changePassword) {
        UserCredentials.changePassword = true;
        UserCredentials.oldPassword = oldPassword;
        UserCredentials.password = password;
        UserCredentials.confirmedPassword = confirmedPassword;
      }
      axios.post(url, UserCredentials).then((res) => {
        if (res.data.success) {
          setMessage(res.data.message);
          const url =
            `https://ibaw-blog-backend.herokuapp.com/userAccount/logout?token=` +
            token.token;
          axios.get(url).then((res) => {
            if (res.data.success) {
              Cookies.remove("token");
              Cookies.remove("username");
              setUserLogged(false);
              history.push("/success");
            }
          });
        } else {
          setErrors({ oldPassword: res.data.message });
        }
      });
    } catch (err) {
      console.log(err);
    }
  };

  const deleteUser = (userId) => {
    try {
      const url = `https://ibaw-blog-backend.herokuapp.com/userAccount/${userId}`;
      const token_ = Cookies.get("token");
      axios.delete(url, { headers: { Authorization: token_ } }).then((res) => {
        if (res.data.success) {
          console.log("user deleted");
        }
      });
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    setUsername(Cookies.get("username"));
    try {
      const userInformation = {
        username: Cookies.get("username"),
      };
      if (userInformation) {
        const url = `https://ibaw-blog-backend.herokuapp.com/userAccount/imageUrl`;
        const token_ = Cookies.get("token");
        axios
          .post(url, userInformation, { headers: { Authorization: token_ } })
          .then((res) => {
            if (res.data.success) {
              if (res.data.imageUrl) {
                setUploadedURL(res.data.imageUrl);
              }
            }
          });
      } else {
        console.log("no username found");
      }
    } catch (err) {
      console.log(err);
    }
  }, []);

  return (
    <>
      <h1 className="profileTitle">
        <i>Profil Informationen verwalten</i>
      </h1>
      <div className="flex-profile">
        <div className="profile-wrapper">
          <ReactTooltip place="top" type="warning" effect="float">
            <span>
              Please note that you will not be able to log in after
              deactivation, but your blog posts will remain
            </span>
          </ReactTooltip>
          <h4 className="profileTitle">Profil activation status</h4>
          <div data-tip="React-tooltip">
            <ToggleButton
              value={profileActivated}
              onToggle={(value) => {
                setProfileActivated(!profileActivated);
              }}
            />
          </div>
          <div className="username">
            <div className="col-3">
              <label className="login-labels">Username:</label>
              <input
                className="effect-1"
                type="Text"
                placeholder="Add your username"
                value={username}
                onChange={(e) => setUsername(e.target.value)}
              />
              <span className="focus-border"></span>
            </div>
          </div>
          {errors.username && <p className="error">{errors.username}</p>}

          <ButtonChangePassword
            color={changePassword ? "grey" : "#644624"}
            opacity={changePassword ? "0.7" : "1"}
            text={changePassword ? "Close" : "Change Password"}
            classes={changePassword ? "collapsible open" : "collapsible closed"}
            setChangePassword={setChangePassword}
            changePassword={changePassword}
          />

          {changePassword ? (
            <div className="password">
              <div className="col-3">
                <label className="login-labels">Old Password:</label>
                <input
                  className="effect-1"
                  type="password"
                  placeholder="Add your old password"
                  value={oldPassword}
                  onChange={(e) => setOldPassword(e.target.value)}
                />
                <span className="focus-border"></span>
              </div>

              {errors.oldPassword && (
                <p className="error">{errors.oldPassword}</p>
              )}

              <div className="col-3">
                <label className="login-labels">New Password:</label>

                <input
                  className="effect-1"
                  type="password"
                  placeholder="Add your new password"
                  value={password}
                  onChange={(e) => setPassword(e.target.value)}
                />
                <span className="focus-border"></span>
              </div>
              {errors.password && <p className="error">{errors.password}</p>}

              <div className="col-3">
                <label className="login-labels">Confirm Password:</label>

                <input
                  className="effect-1"
                  type="password"
                  placeholder="Confirm your new password"
                  value={confirmedPassword}
                  onChange={(e) => setConfirmedPassword(e.target.value)}
                />
                <span className="focus-border"></span>
              </div>
              {errors.password && <p className="error">{errors.password}</p>}

              {errors.confirmedPassword && (
                <p className="error">{errors.confirmedPassword}</p>
              )}
            </div>
          ) : (
            ""
          )}
          <button onClick={saveCredentials}>Save</button>

          <p className="error">{message}</p>
        </div>
        <div className="image-wrapper">
          <UploadProfileImage
            uploadedURL={uploadedURL}
            setUploadedURL={setUploadedURL}
          />
        </div>
      </div>
    </>
  );
};

export default Profile;
