import React from "react";
import { useLocation } from "react-router";
import styledBlogEntry from "./BlogEntry.css";
import ReactHtmlParser from "react-html-parser";
import Comments from "../Comments/Comments.js";

const BlogEntry = () => {
  const location = useLocation();
  const blog = location.state.blog;

  return (
    <>
      <div className="blogTitle">
        <h3>{blog.title}</h3>
        {blog.imageUrl && (
          <img alt="Starter" className="blogImage" src={blog.imageUrl}></img>
        )}
      </div>

      <div className="blog-content">{ReactHtmlParser(blog.content)}</div>
      <Comments blog={blog} />
    </>
  );
};

export default BlogEntry;
