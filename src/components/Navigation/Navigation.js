import { useState, useEffect } from "react";
import styledNavigation from "./Navigation.css";
import axios from "axios";
import { Link } from "react-router-dom";
import Cookies from "js-cookie";

const Navigation = ({
  loginMask,
  setLoginMask,
  userLogged,
  setUserLogged,
  loggedUserName,
  profileImageURL,
}) => {
  // const [token, setToken] = useState("");
  const [click, setClick] = useState(false);

  const handleClick = () => {
    setClick(!click);
  };

  const closeHamburgerMenu = () => {
    if (window.innerWidth <= 620) {
      setClick(!click);
    }
  };

  const closeMobileMenu = () => setClick(false);

  // useEffect(() => {
  //   try {
  //     setToken(Cookies.get("token"));
  //   } catch (err) {
  //     console.log(err);
  //   }
  // }, []);

  const logoutUser = () => {
    try {
      const currentToken = Cookies.get("token");
      const url =
        `https://ibaw-blog-backend.herokuapp.com/userAccount/logout?token=` +
        currentToken.token;
      axios.get(url).then((res) => {
        if (res.data.success) {
          Cookies.remove("token");
          Cookies.remove("username");
          setUserLogged(false);
        }
      });
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <div className="navbar">
      <div className="nav-container">
        <nav className="main-nav">
          <a href="https://ibaw-blog-frontend.herokuapp.com/">
            <img alt="Logo" src="img/Logo.png"></img>
          </a>
          <ul
            className={
              click
                ? userLogged
                  ? "nav-options active loggedIn"
                  : "nav-options active loggedOut"
                : "main-menu"
            }
          >
            <li className="option" onClick={closeHamburgerMenu}>
              <Link to={"/blogs"}>Blogs</Link>
            </li>
            {userLogged ? (
              <>
                <li className="option" onClick={closeHamburgerMenu}>
                  <Link to={"/createblog"}>Create blog</Link>
                </li>
                <li className="option" onClick={closeHamburgerMenu}>
                  <Link to={"/profile"}>My profile</Link>
                </li>
                <li className="option" onClick={closeHamburgerMenu}>
                  <Link to={"/myBlogs"}>My blogs</Link>
                </li>
              </>
            ) : (
              ""
            )}

            {!userLogged ? (
              <>
                {" "}
                <li className="option mobile-option" onClick={closeMobileMenu}>
                  <Link className="sign-in" to={"/login"}>
                    Login
                  </Link>
                </li>
                <li className="option mobile-option" onClick={closeMobileMenu}>
                  <Link className="sign-up" to={"/signup"}>
                    Sign Up
                  </Link>
                </li>{" "}
              </>
            ) : (
              <>
                <li className="option mobile-option" onClick={closeMobileMenu}>
                  <Link
                    onClick={() => logoutUser()}
                    className="sign-up"
                    to={"/"}
                  >
                    Logout
                  </Link>
                </li>
              </>
            )}
          </ul>
          {!userLogged ? (
            <>
              <div className="option login nav-options">
                <Link to={"/login"}>Login</Link>
              </div>
              <div className="option signup nav-options">
                <Link to={"/signup"}>Sign Up</Link>
              </div>
            </>
          ) : (
            <>
              <div className="option nav-options">
                <Link to={"/"}>
                  <button onClick={() => logoutUser()}>Logout</button>
                </Link>
              </div>
              {profileImageURL && (
                <div className="option nav-options">
                  <img
                    src={profileImageURL}
                    alt="Profile Image"
                    id="img"
                    className="navigation-img"
                  ></img>
                </div>
              )}
              <div className="option nav-options">
                <p>{loggedUserName}</p>
              </div>
            </>
          )}
          <div className="mobile-menu" onClick={handleClick}>
            {click ? (
              <i className="fas fa-times fa-2x menu-icon"></i>
            ) : (
              <i className="fas fa-bars fa-2x menu-icon"></i>
            )}
          </div>
        </nav>
      </div>
    </div>
  );
};

export default Navigation;
