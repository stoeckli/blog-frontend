import React from "react";
import styledSuccess from "./Success.css";

const Success = ({ text }) => {
  return (
    <article>
      <div className="card">
        <div className="icon">
          <i className="checkmark">✓</i>
        </div>
        <h1>Success</h1>
        <p>{text}</p>
      </div>
    </article>
  );
};

export default Success;
