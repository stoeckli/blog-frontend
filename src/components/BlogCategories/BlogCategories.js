import React from "react";
import styledBlogCategories from "./BlogCategories.css";

const BlogCategories = ({ currentCategroy, setCurrentCategroy }) => {
  return (
    <>
      <div>
        <div className="select-box">
          <div className="select-box__current" tabIndex="1">
            <div className="select-box__value">
              <input
                className="select-box__input"
                type="radio"
                id="0"
                value="All"
                name="Ben"
                checked="checked"
                onChange={(e) => setCurrentCategroy(e.target.value)}
              />
              <p className="select-box__input-text">{currentCategroy}</p>
            </div>
            <div className="select-box__value">
              <input
                className="select-box__input"
                type="radio"
                id="1"
                value="Soccer"
                name="Ben"
                onChange={(e) => setCurrentCategroy(e.target.value)}
              />
              <p className="select-box__input-text">{currentCategroy}</p>
            </div>
            <div className="select-box__value">
              <input
                className="select-box__input"
                type="radio"
                id="2"
                value="Running"
                name="Ben"
                onChange={(e) => setCurrentCategroy(e.target.value)}
              />
              <p className="select-box__input-text">{currentCategroy}</p>
            </div>
            <div className="select-box__value">
              <input
                className="select-box__input"
                type="radio"
                id="3"
                value="Cycling"
                name="Ben"
                onChange={(e) => setCurrentCategroy(e.target.value)}
              />
              <p className="select-box__input-text">{currentCategroy}</p>
            </div>
            <div className="select-box__value">
              <input
                className="select-box__input"
                type="radio"
                id="4"
                value="Hockey"
                name="Ben"
                onChange={(e) => setCurrentCategroy(e.target.value)}
              />
              <p className="select-box__input-text">{currentCategroy}</p>
            </div>
            <img
              className="select-box__icon"
              src="http://cdn.onlinewebfonts.com/svg/img_295694.svg"
              alt="Arrow Icon"
              aria-hidden="true"
            />
          </div>
          <ul className="select-box__list">
            <li>
              <label
                className="select-box__option"
                htmlFor="0"
                aria-hidden="false"
              >
                All
              </label>
            </li>
            <li>
              <label
                className="select-box__option"
                htmlFor="1"
                aria-hidden="false"
              >
                Soccer
              </label>
            </li>
            <li>
              <label
                className="select-box__option"
                htmlFor="2"
                aria-hidden="false"
              >
                Running
              </label>
            </li>
            <li>
              <label
                className="select-box__option"
                htmlFor="3"
                aria-hidden="false"
              >
                Cycling
              </label>
            </li>
            <li>
              <label
                className="select-box__option"
                htmlFor="4"
                aria-hidden="false"
              >
                Hockey
              </label>
            </li>
          </ul>
        </div>
      </div>
    </>
  );
};

export default BlogCategories;
