import React, { useState, useEffect } from "react";
import axios from "axios";
import Comment from "../Comment/Comment.js";
import NewComment from "../NewComment/NewComment.js";
import styledComments from "./Comments.css";
import Cookies from "js-cookie";

export const Comments = ({ blog }) => {
  const [currentBlog, setCurrentBlog] = useState(blog);
  const [userLoggedIn, setUserLoggedIn] = useState(false);

  useEffect(() => {
    const token = Cookies.get("token");
    if (token) {
      setUserLoggedIn(true);
    }
  }, []);

  const addComment = (newComment, blog) => {
    const url = `https://ibaw-blog-backend.herokuapp.com/blog/${blog._id}`;
    const comment = {
      text: newComment.newComment,
      author: newComment.name,
    };
    const token = Cookies.get("token");
    try {
      setUserLoggedIn(true);
      axios
        .post(url, comment, { headers: { Authorization: token } })
        .then((res) => {
          setCurrentBlog(res.data);
        });
    } catch (err) {
      setUserLoggedIn(false);
    }
  };

  const deleteComment = (blogId, commentId) => {
    const url = `https://ibaw-blog-backend.herokuapp.com/blog/${blogId}/${commentId}`;
    const token = Cookies.get("token");
    axios.delete(url, { headers: { Authorization: token } }).then((res) => {
      setCurrentBlog(res.data);
    });
  };

  return (
    <>
      {currentBlog.comments.length > 0 ? (
        <div className="comment-area">
          <article className="comments">
            {currentBlog.comments.length > 0 && <h4>Comments:</h4>}
            {currentBlog.comments.map((comment) => (
              <Comment
                comment={comment}
                blog={blog}
                deleteComment={deleteComment}
              />
            ))}
          </article>
          {userLoggedIn ? (
            <NewComment addComment={addComment} blog={blog} />
          ) : (
            ""
          )}
        </div>
      ) : (
        <div className="comment-area">
          {/* <article className="comments">
            {currentBlog.comments.length > 0 && <h4>Comments:</h4>}
            {currentBlog.comments.map((comment) => (
              <Comment
                comment={comment}
                blog={blog}
                deleteComment={deleteComment}
              />
            ))}
          </article> */}
          {userLoggedIn ? (
            <NewComment addComment={addComment} blog={blog} />
          ) : (
            ""
          )}
        </div>
      )}
    </>
  );
};

export default Comments;
