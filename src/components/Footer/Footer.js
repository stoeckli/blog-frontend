import React from "react";
import styledFooter from "./Footer.css";
import { Link } from "react-router-dom";

const Footer = () => {
  return (
    <footer className="footer">
      <div className="l-footer">
        <h3>
          <img alt="Footer-Logo" src="img/Logo_W.png"></img>
        </h3>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laboriosam
          atque recusandae in sit sunt molestiae aliquid fugit. Mollitia eaque
          tempore iure sit nobis? Vitae nemo, optio maiores numquam quis
          recusandae.
        </p>
      </div>
      <ul className="r-footer">
        <li>
          <h3>Social</h3>
          <ul className="box">
            <li>
              <a target="_blank" rel="noopener" href="https://facebook.com">
                Facebook
              </a>
            </li>
            <li>
              <a target="_blank" rel="noopener" href="https://twitter.com/">
                Twitter
              </a>
            </li>
            <li>
              <a
                target="_blank"
                rel="noopener"
                href="https://www.pinterest.ch/"
              >
                Pinterest
              </a>
            </li>
            <li>
              <a target="_blank" rel="noopener" href="https://dribbble.com/">
                Dribbble
              </a>
            </li>
          </ul>
        </li>
        <li className="features">
          <h3>Information</h3>
          <ul className="box">
            <li>
              <Link to={"/blogs"}>Blog</Link>
            </li>
            <li>
              <a href="#">Pricing</a>
            </li>
            <li>
              <a href="#">Sales</a>
            </li>
          </ul>
        </li>
        <li>
          <h3>Legal</h3>
          <ul className="box">
            <li>
              <a href="#">Privacy Policy</a>
            </li>
            <li>
              <a href="#">Terms of Use</a>
            </li>
            <li>
              <a href="#">Contract</a>
            </li>
          </ul>
        </li>
      </ul>
      <div className="b-footer">
        <p>All rights reserved by ©CompanyName 2022 </p>
      </div>
    </footer>
  );
};
export default Footer;
