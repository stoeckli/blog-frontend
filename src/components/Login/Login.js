import React, { useState } from "react";
import styledLogin from "./Login.css";
import Validation from "../Validation/Validation";
import axios from "axios";
import { Redirect } from "react-router";
import { Link } from "react-router-dom";
import Cookies from "js-cookie";

const Login = ({
  submitForm,
  setloggedUserName,
  userLogged,
  setUserLogged,
  setReloadPage,
}) => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [errors, setErrors] = useState({});
  const [loginFailed, setLoginFailed] = useState("");
  const [loggedIn, setLoggedIn] = useState(false);
  const [token, setToken] = useState("");

  const handlerForSubmit = (e) => {
    e.preventDefault();
    setErrors(Validation({ email, password }));
    loginUser({ email, password });
    setEmail("");
    setPassword("");
  };

  const loginUser = (user) => {
    const url = `https://ibaw-blog-backend.herokuapp.com/userAccount/login`;
    const UserCredentials = {
      email: user.email,
      password: user.password,
    };

    axios.post(url, UserCredentials).then((res) => {
      if (res.data.success) {
        Cookies.set("token", res.data.token, {
          expires: 1 / 6,
        });
        Cookies.set("username", res.data.username, {
          expires: 1 / 6,
        });
        setToken(res.data.token);
        setLoggedIn(true);
        setUserLogged(true);
        window.location.reload();
      } else {
        setLoginFailed(res.data.message);
      }
    });
  };

  return (
    <>
      {loggedIn ? <Redirect to={"/"} /> : ""}

      <div className="flex-wrapper">
        <div className="flex-login-form">
          <div className="login-box">
            <div className="imgcontainer">
              <img
                src="https://media.geeksforgeeks.org/wp-content/cdn-uploads/20190710102234/download3.png"
                alt="Avatar"
                className="avatar"
              />
            </div>
            <div className="login-forms">
              <div className="input-div username">
                <div className="i">
                  <i className="fas fa-user"></i>
                </div>
                <div className="col-3">
                  <label className="login-labels">E-Mail:</label>
                  <input
                    className="effect-1"
                    type="Text"
                    placeholder="Add your email"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                  />
                  <span className="focus-border"></span>
                </div>
              </div>
              {errors.email && <p className="error">{errors.email}</p>}
            </div>
            <div className="login-forms">
              <div className="input-div password">
                <div className="i">
                  <i className="fas fa-lock"></i>
                </div>
                <div className="col-3">
                  <label className="login-labels">Password:</label>
                  <input
                    className="effect-1"
                    type="password"
                    placeholder="Add your password"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                  />
                  <span className="focus-border"></span>
                </div>
              </div>
              {errors.password && <p className="error">{errors.password}</p>}
            </div>
            <div className="login-forms">
              <button className="login" onClick={handlerForSubmit}>
                Login
              </button>
              <div className="forgotPassword">
                <p className="forgot">
                  <Link to={"/forgot"}>Forgot password?</Link>
                </p>
              </div>
            </div>
            {loginFailed && <p className="error">{loginFailed}</p>}
          </div>
        </div>
      </div>
    </>
  );
};

export default Login;
