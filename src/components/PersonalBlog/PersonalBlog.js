import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";

import styledPersonalBlog from "./PersonalBlog.css";

const PersonalBlog = ({ blogs, blog, onClick }) => {
  const [category, setCategory] = useState("");
  const [blogEntryClicked, setBlogEntryClicked] = useState(false);

  useEffect(() => {
    setCategory(blog.category);
  }, []);

  return (
    <>
      {blogEntryClicked ? (
        ""
      ) : (
        <Link
          to={{
            pathname: "/UpdateBlogEntry",
            state: {
              blog: blog,
            },
          }}
          blog={"Personal Blog"}
        >
          <div className="personalBlogEntries" href="/BlogEntry">
            <p className="category">{blog.category}</p>
            {category === "Soccer" ? (
              <i className="small-icons far fa-futbol"></i>
            ) : (
              ""
            )}
            {category === "Running" ? (
              <i className="small-icons fas fa-running"></i>
            ) : (
              ""
            )}
            {category === "Cycling" ? (
              <i className="small-icons fas fa-biking"></i>
            ) : (
              ""
            )}
            {category === "Hockey" ? (
              <i className="small-icons fas fa-hockey-puck"></i>
            ) : (
              ""
            )}

            <h3>{blog.title}</h3>
          </div>
        </Link>
      )}
    </>
  );
};

export default PersonalBlog;
