import React from "react";
import { useState } from "react";
import styledHome from "./Home.css";

const Home = () => {
  return (
    <>
      <div className="hero-image"></div>
      <div className="home-container">
        <div className="home-box-ziel">
          <h3>Our goal</h3>
          <div>
            <p className="home-box-content">
              Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas
              porttitor congue massa. Fusce posuere, magna sed pulvinar
              ultricies, purus lectus malesuada libero, sit amet commodo magna
              eros quis urna. Lorem ipsum dolor sit amet, consectetuer
              adipiscing elit. Maecenas porttitor congue massa. Fusce posuere,
              magna sed pulvinar ultricies, purus lectus malesuada libero, sit
              amet commodo magna eros quis urna. Lorem ipsum dolor sit amet,
              consectetuer adipiscing elit. Maecenas porttitor congue massa.
              Fusce posuere, magna sed pulvinar ultricies, purus lectus
              malesuada libero, sit amet commodo magna eros quis urna.
            </p>
          </div>
        </div>
        <h1 className="founders">The founders</h1>
        <section className="grid-blogger">
          <div className="blogger">
            <div className="placeholder-man"></div>
            <p className="bloggerName">Lutz Schüßler</p>
            <p>
              Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas
              porttitor congue massa. Fusce posuere, magna sed pulvinar
              ultricies, purus lectus malesuada libero, sit amet commodo magna
              eros quis urna.
            </p>
          </div>
          <div className="blogger">
            <div className="placeholder-man"></div>
            <p className="bloggerName">Samantha Szabo</p>
            <p>
              Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas
              porttitor congue massa. Fusce posuere, magna sed pulvinar
              ultricies, purus lectus malesuada libero, sit amet commodo magna
              eros quis urna.
            </p>
          </div>
          <div className="blogger">
            <div className="placeholder-man"></div>
            <p className="bloggerName">Catja Lieske</p>
            <p>
              Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas
              porttitor congue massa. Fusce posuere, magna sed pulvinar
              ultricies, purus lectus malesuada libero, sit amet commodo magna
              eros quis urna.
            </p>
          </div>
          <div className="blogger">
            <div className="placeholder-man"></div>
            <p className="bloggerName">Fabian Waltz</p>
            <p>
              Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas
              porttitor congue massa. Fusce posuere, magna sed pulvinar
              ultricies, purus lectus malesuada libero, sit amet commodo magna
              eros quis urna.
            </p>
          </div>
        </section>
      </div>
    </>
  );
};

export default Home;
