import React, { useState } from "react";
import styledCreateBlogEntry from "./CreateBlogEntry.css";
import axios from "axios";
import { TextEditorCreate } from "../TextEditor/TextEditor.js";
import Cookies from "js-cookie";
import io from "socket.io-client";
import UploadImageForm from "../UploadImageForm/UploadImageForm.js";
import DeleteImageForm from "../DeleteImageForm/DeleteImageForm.js";
import { Redirect } from "react-router";

const socket = io.connect("https://ibaw-blog-backend.herokuapp.com/");

export const CreateBlogEntry = () => {
  const [blogTitle, setBlogTitle] = useState("");
  const [blogContent, setBlogContent] = useState("");
  const [blogCategory, setBlogCategory] = useState("Soccer");
  const [uploadedURL, setUploadedURL] = useState(null);
  const [uploadedFileName, setUploadedFileName] = useState(null);
  const [errorMessage, setErrorMessage] = useState("");
  const [blogCreated, setBlogCreated] = useState(false);

  const newEntity = (e) => {
    if (!blogTitle) {
      alert("Please add a blog title");
      return;
    }
    addBlogEntry({ blogTitle, blogContent, blogCategory });
    setBlogTitle("");
    setBlogContent("");
    setErrorMessage("");
  };

  const addBlogEntry = (blogEntries) => {
    try {
      const url = `https://ibaw-blog-backend.herokuapp.com/blog/`;
      const blogEntryValues = {
        title: blogEntries.blogTitle,
        content: blogEntries.blogContent,
        category: blogEntries.blogCategory,
        imageUrl: uploadedURL,
      };
      const token = Cookies.get("token");

      if (token) {
        axios
          .post(url, blogEntryValues, { headers: { Authorization: token } })
          .then((res) => {
            if (res.data.success) {
              if (res.data.blog == null) {
                setErrorMessage(
                  "Error happened. Blog has not been created. Please reload the page"
                );
                return;
              }
              const blogId = res.data.blog._id;

              const socketEmit = async () => {
                await socket.emit("message", {
                  blogId,
                });
              };
              socketEmit();
              setBlogCreated(true);
              // window.location.reload();
            }
          });
      }
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <>
      {blogCreated ? <Redirect to={"/blogs"} /> : ""}
      <h1 className="line">Create a new Blog article</h1>
      <div className="flex-wrapper">
        <div className="flex-login-form">
          <div className="login-box">
            <div className="create-blog">
              <div className="login-forms select-wrapper">
                <label className="blog-label">Category:</label>
                <select
                  className="test"
                  onChange={(e) => {
                    const selectedSport = e.target.value;
                    setBlogCategory(selectedSport);
                  }}
                >
                  <option value="Soccer">Soccer</option>
                  <option value="Running">Running</option>
                  <option value="Cycling">Cycling</option>
                  <option value="Hockey">Hockey</option>
                </select>
              </div>
              <div className="login-forms">
                <label className="blog-label">Blog title:</label>
                <input
                  type="Text"
                  placeholder="Add a title for your blog"
                  value={blogTitle}
                  onChange={(e) => setBlogTitle(e.target.value)}
                />
              </div>

              <div className="login-forms TextEditor">
                <TextEditorCreate
                  blogContent={blogContent}
                  setBlogContent={setBlogContent}
                />
              </div>
              {!uploadedURL ? (
                <UploadImageForm
                  setUploadedURL={setUploadedURL}
                  setUploadedFileName={setUploadedFileName}
                />
              ) : (
                <DeleteImageForm
                  uploadedURL={uploadedURL}
                  setUploadedURL={setUploadedURL}
                  uploadedFileName={uploadedFileName}
                />
              )}
              {uploadedURL && (
                <img
                  alt="Upload Preview"
                  className="uploadedImage"
                  src={uploadedURL}
                ></img>
              )}

              <div className="login-forms">
                <button onClick={newEntity}>Create Blog</button>
              </div>
              {errorMessage && <p className="error">{errorMessage}</p>}
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default CreateBlogEntry;
