import React, { useState } from "react";
import Validation from "../Validation/Validation";
import axios from "axios";
import { useParams } from "react-router-dom";
import Success from "../Success/Success";

export const ResetPassword = () => {
  const [password, setPassword] = useState("");
  const [confirmedPassword, setConfirmedPassword] = useState("");
  const [errors, setErrors] = useState({});
  const { id } = useParams();
  const [resetSuccessfull, setResetSuccessfull] = useState(false);
  const [successfullText, setSuccessfullText] = useState("");

  const handlerForSubmit = (e) => {
    e.preventDefault();
    setErrors(Validation({ password, confirmedPassword }));

    setPassword("");
    setConfirmedPassword("");
    if (password === confirmedPassword) {
      resetPassword(password);
    }
  };
  const resetPassword = (password) => {
    try {
      const url = `https://ibaw-blog-backend.herokuapp.com/userAccount/reset/${id}`;
      const UserCredentials = {
        password: password,
      };

      axios.post(url, UserCredentials).then((res) => {
        if (res.data.success) {
          setResetSuccessfull(true);
          setSuccessfullText("New Password has been set");
        } else {
          setErrors(res.data.message);
        }
      });
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <>
      {resetSuccessfull ? (
        <Success text={successfullText} />
      ) : (
        <div className="flex-wrapper">
          <div className="flex-login-form">
            <div className="login-box">
              <div className="login-forms">
                <label className="login-labels">Enter new Password:</label>
                <input
                  type="password"
                  placeholder="Add your new password"
                  value={password}
                  onChange={(e) => setPassword(e.target.value)}
                />
                {errors.password && <p className="error">{errors.password}</p>}
              </div>
              <div className="login-forms">
                <label className="login-labels">Confirm Password:</label>
                <input
                  type="password"
                  placeholder="Confirm password"
                  value={confirmedPassword}
                  onChange={(e) => setConfirmedPassword(e.target.value)}
                />
                {errors.confirmedPassword && (
                  <p className="error">{errors.confirmedPassword}</p>
                )}
              </div>

              <div className="login-forms">
                <button className="login" onClick={handlerForSubmit}>
                  Submit
                </button>
              </div>
            </div>
          </div>
        </div>
      )}
    </>
  );
};
