import React, { useState } from "react";
import styledNewComment from "./NewComment.css";

const NewComment = ({ addComment, blog }) => {
  // const [name, setName] = useState("");
  const [newComment, setNewComment] = useState("");

  const onSubmit = (e) => {
    if (!newComment) {
      alert("Please add your comment");
      return;
    }
    addComment({ newComment }, blog);
    // setName("");
    setNewComment("");
  };

  return (
    <>
      <div className="flex-comment-area">
        <h4>Schreiben Sie ihren Kommentar:</h4>
        {/* <div className="comment-forms">
          <label className="comment-labels">Name:</label>
          <textarea
            className="author"
            type="Text"
            placeholder="Add your name"
            value={name}
            onChange={(e) => setName(e.target.value)}
          ></textarea>
        </div> */}
        <div className="comment-forms">
          <label className="comment-labels">Kommentar:</label>
          <textarea
            className="new-comment"
            type="Text"
            placeholder="Add your comment"
            value={newComment}
            onChange={(e) => setNewComment(e.target.value)}
          ></textarea>
        </div>
        <button onClick={onSubmit}>Kommentar abschicken</button>
      </div>
    </>
  );
};

export default NewComment;
