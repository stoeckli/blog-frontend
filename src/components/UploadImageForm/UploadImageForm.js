import React, { useState } from "react";
import ProgressBar from "../ProgressBar/ProgressBar.js";
import styledUploadImageForm from "../UploadImageForm/UploadImageForm.css";

const UploadImageForm = ({ setUploadedURL, setUploadedFileName }) => {
  const [file, setFile] = useState(null);
  const [error, setError] = useState(null);

  const types = ["image/png", "image/jpg", "image/jpeg"];

  const imageHandler = (e) => {
    let selected = e.target.files[0];
    if (selected && types.includes(selected.type)) {
      setFile(selected);
      setError("");
    } else {
      setFile(null);
      setError("Please select an image file (png or jpeg)");
    }
  };

  return (
    <form className="uploadImage">
      <input
        type="file"
        name="image-upload"
        id="upload-id"
        className="imageSelector"
        onChange={imageHandler}
        accept="image/*"
      ></input>
      <div className="label">
        <label htmlFor="upload-id" className="image-upload">
          <i className="fas fa-upload"></i>
          Choose image
        </label>
      </div>
      <div className="output">
        {error && <div className="error">{error}</div>}

        {file && (
          <ProgressBar
            file={file}
            setFile={setFile}
            setUploadedURL={setUploadedURL}
            setUploadedFileName={setUploadedFileName}
            foldername="files"
          />
        )}
      </div>
    </form>
  );
};

export default UploadImageForm;
