import React, { useState, useEffect } from "react";
import styledUpdateBlogEntry from "./UpdateBlogEntry.css";
import axios from "axios";
import { TextEditorUpdate } from "../TextEditor/TextEditor.js";
import UploadImageForm from "../UploadImageForm/UploadImageForm.js";
import DeleteImageForm from "../DeleteImageForm/DeleteImageForm.js";
import { useLocation } from "react-router";
import Success from "../Success/Success";
import Cookies from "js-cookie";

export const UpdateBlogEntry = () => {
  const [blogTitle, setBlogTitle] = useState("");
  const [blogContent, setBlogContent] = useState("");
  const [blogCategory, setBlogCategory] = useState("Soccer");
  const [blog, setBlog] = useState();
  const [uploadedURL, setUploadedURL] = useState(null);
  const [uploadedFileName, setUploadedFileName] = useState(null);
  const [updateSuccessfull, setUpdateSuccessfull] = useState(false);
  const [successfullText, setSuccessfullText] = useState("");

  const location = useLocation();

  useEffect(() => {
    const blog = location.state.blog;
    setBlogTitle(blog.title);
    setBlogContent(blog.content);
    setBlogCategory(blog.category);
    setBlog(blog);
    if (blog.imageUrl) setUploadedURL(blog.imageUrl);
  }, [location]);

  const updateEntity = (e) => {
    if (!blogTitle) {
      alert("Please add a blog title");
      return;
    }

    updateBlogEntry({ blogTitle, blogContent, blogCategory });
    setBlogTitle("");
    setBlogContent("");
  };

  const updateBlogEntry = (blogEntries) => {
    try {
      const url = `https://ibaw-blog-backend.herokuapp.com/blog/${blog._id}`;
      const blogEntryValues = {
        title: blogEntries.blogTitle,
        content: blogEntries.blogContent,
        category: blogEntries.blogCategory,
        imageUrl: uploadedURL,
      };
      const token = Cookies.get("token");

      axios
        .put(url, blogEntryValues, { headers: { Authorization: token } })
        .then((res) => {
          if (res.data.success) {
            setSuccessfullText(res.data.message);
            setUpdateSuccessfull(!updateSuccessfull);
          }
        });
    } catch (err) {
      console.log(err);
    }
  };

  const deleteBlogEntry = () => {
    const token = Cookies.get("token");
    try {
      const url = `https://ibaw-blog-backend.herokuapp.com/blog/${blog._id}`;
      axios.delete(url, { headers: { Authorization: token } }).then((res) => {
        if (res.data.success) {
          setSuccessfullText(res.data.message);
          setUpdateSuccessfull(!updateSuccessfull);
        }
      });
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <>
      {updateSuccessfull ? (
        <Success text={successfullText} />
      ) : (
        <>
          <h1 className="line">Update Blog article</h1>
          <div className="flex-wrapper">
            <div className="flex-login-form">
              <div className="login-box">
                <div className="remove-label" onClick={deleteBlogEntry}>
                  <label className="remove-blog">
                    <i className="far fa-trash-alt"></i>
                    Remove Blog
                  </label>
                </div>
                <div className="create-blog">
                  <div className="login-forms select-wrapper">
                    <label className="blog-label">Category:</label>
                    <select
                      value={blogCategory}
                      onChange={(e) => {
                        const selectedSport = e.target.value;
                        setBlogCategory(selectedSport);
                      }}
                    >
                      <option value="Soccer">Soccer</option>
                      <option value="Running">Running</option>
                      <option value="Cycling">Cycling</option>
                      <option value="Hockey">Hockey</option>
                    </select>
                  </div>
                  <div className="login-forms">
                    <label className="blog-label">Blog title:</label>
                    <input
                      type="Text"
                      placeholder="Add a title for your blog"
                      value={blogTitle}
                      onChange={(e) => setBlogTitle(e.target.value)}
                    />
                  </div>

                  <div className="login-forms TextEditor">
                    <TextEditorUpdate
                      blogContent={blogContent}
                      setBlogContent={setBlogContent}
                    />
                  </div>
                  {!uploadedURL ? (
                    <UploadImageForm
                      setUploadedURL={setUploadedURL}
                      setUploadedFileName={setUploadedFileName}
                    />
                  ) : (
                    <DeleteImageForm
                      uploadedURL={uploadedURL}
                      setUploadedURL={setUploadedURL}
                      uploadedFileName={uploadedFileName}
                    />
                  )}
                  {uploadedURL && (
                    <img
                      alt="uploaded preview"
                      className="uploadedImage"
                      src={uploadedURL}
                    ></img>
                  )}

                  <div className="login-forms">
                    <button onClick={updateEntity}>Update Blog Entry</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </>
      )}
    </>
  );
};

export default UpdateBlogEntry;
