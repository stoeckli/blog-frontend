import React, { useEffect, useState } from "react";
import { FaTimes } from "react-icons/fa";
import styledComment from "./Comment.css";
import Cookies from "js-cookie";
import moment from "moment";

const Comment = ({ comment, blog, deleteComment }) => {
  const [myComment, setMyComment] = useState(false);
  const [username, setUsername] = useState(Cookies.get("username"));

  return (
    <>
      <article className="flex-area">
        <article className="flex-comment-list">
          <div className="flex-title">
            {comment.author == null ? (
              <p className="comment-list-name">Deleted User</p>
            ) : (
              <p className="comment-list-name">{comment.author.username}</p>
            )}

            <p className="creation-date">
              {moment(comment.createdAt).format("MMMM Do YYYY, h:mm a")}
            </p>
          </div>
          <div>{comment.text}</div>
        </article>
        {username == comment.author.username ? (
          <div
            className="delete-option"
            onClick={() => deleteComment(blog._id, comment._id)}
          >
            <i className="far fa-trash-alt fa-2x"></i>
          </div>
        ) : (
          <div className="foreign-comment"></div>
        )}
      </article>
    </>
  );
};

export default Comment;
