import React, { useState } from "react";
import deleteStorage from "../../utils/deleteStorage.js";
import styledDeleteImageForm from "./DeleteImageForm.css";

const DeleteImageForm = ({ setUploadedURL, uploadedFileName }) => {
  const types = ["image/png", "image/jpg", "image/jpeg"];

  const handlerForSubmit = (e) => {
    e.preventDefault();
    deleteStorage(uploadedFileName);
    setUploadedURL(null);
  };

  return (
    <form>
      <button classname="deleteFirebaseImage" onClick={handlerForSubmit}>
        <i className="far fa-trash-alt"></i>
        Delete Image
      </button>
    </form>
  );
};

export default DeleteImageForm;
