import styledButtonChangePassword from "./ButtonChangePassword.css";

const Button = ({
  color,
  text,
  setChangePassword,
  changePassword,
  classes,
  opacity,
}) => {
  return (
    <>
      <button
        type="button"
        onClick={() => setChangePassword(!changePassword)}
        style={{ backgroundColor: color, opacity: opacity }}
        className={classes}
      >
        {text}
      </button>
    </>
  );
};

export default Button;
