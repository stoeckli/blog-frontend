import React, { useEffect } from "react";
import useStorage from "../../utils/useStorage.js";
import styledProgressBar from "./ProgressBar.css";

const ProgressBar = ({
  file,
  setFile,
  setUploadedURL,
  setUploadedFileName,
  foldername,
}) => {
  const { url, progress } = useStorage(file, foldername);

  useEffect(() => {
    if (url) {
      setUploadedFileName(file.name);
      setFile(null);
      setUploadedURL(url);
    }
  }, [url, setFile]);

  return (
    <>
      <div className="progress-bar" style={{ width: progress + `%` }}></div>
    </>
  );
};

export default ProgressBar;
