const Validation = (values) => {
  let errors = {};
  if (!values.username) {
    errors.username = "Username is required";
  }
  if (!values.email) {
    errors.email = "Email is required";
  } else if (!/\S+@\S+\.\S+/.test(values.email)) {
    errors.email = "Email is invalid";
  }
  if (!values.password) {
    errors.password = "Password is required";
  } else if (values.oldPassword) {
    if (values.oldPassword.length < 5) {
      errors.oldPassword = "Old Password must be more then 5 characters";
    }
  } else if (values.password.length < 5) {
    errors.password = "Password must be more then 5 characters";
  } else if (values.password != values.confirmedPassword) {
    errors.confirmedPassword =
      "Password and confirmed password are not the same";
  }

  return errors;
};

export default Validation;
