const ProfileValidation = (values) => {
  let errors = {};
  if (!values.username) {
    errors.username = "Username is required";
  }
  if (!values.password) {
    errors.password = "Password is required";
  }
  if (!values.oldPassword) {
    errors.oldPassword = "Password is required";
  }
  if (values.oldPassword) {
    if (values.oldPassword.length < 5) {
      errors.oldPassword = "Old Password must be more then 5 characters";
    }
  }
  if (values.password) {
    console.log(values.password);
    if (values.password.length < 5) {
      errors.password = "Password must be more then 5 characters";
    }
  }
  if (values.password !== values.confirmedPassword) {
    errors.confirmedPassword =
      "Password and confirmed password are not the same";
  }

  return errors;
};

export default ProfileValidation;
