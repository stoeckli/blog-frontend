import React, { useState } from "react";
import Validation from "../Validation/Validation";
import axios from "axios";

const ForgotPassword = () => {
  const [email, setEmail] = useState("");
  const [errors, setErrors] = useState({});
  const [wrongEmail, setWrongEmail] = useState(false);
  const [infoMessage, setInfoMessage] = useState("");

  const handlerForSubmit = (e) => {
    e.preventDefault();
    setErrors(Validation({ email }));
    setEmail("");
    checkEmail(email);
    setWrongEmail(false);
    //setErrorMessage("res.data.message");
  };

  const checkEmail = (email) => {
    try {
      const url = `https://ibaw-blog-backend.herokuapp.com/userAccount/forgot`;
      const UserCredentials = {
        email: email,
      };

      axios.post(url, UserCredentials).then((res) => {
        if (!res.data.success) {
          setWrongEmail(true);
          setInfoMessage(res.data.message);
        } else {
          setWrongEmail(true);
          setInfoMessage(res.data.message);
        }
      });
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <>
      <div className="flex-wrapper">
        <div className="flex-login-form">
          <div className="login-box">
            <div className="login-forms">
              <div className="input-div username">
                <div className="i">
                  <i className="fas fa-user"></i>
                </div>
                <div className="col-3">
                  <label className="login-labels">E-Mail:</label>
                  <input
                    className="effect-1"
                    type="Text"
                    placeholder="Add your email"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                  />
                  <span className="focus-border"></span>
                </div>
                <p className="placeholder"></p>
                {errors.email && <p className="error">{errors.email}</p>}
              </div>
            </div>

            <div className="login-forms">
              <button className="login" onClick={handlerForSubmit}>
                Submit
              </button>
            </div>
            {wrongEmail && <p className="info">{infoMessage}</p>}
          </div>
        </div>
      </div>
    </>
  );
};

export default ForgotPassword;
