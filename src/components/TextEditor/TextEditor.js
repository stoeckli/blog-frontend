import React, { useEffect, useState } from "react";
import { Editor } from "react-draft-wysiwyg";
import {
  EditorState,
  convertToRaw,
  ContentState,
  convertFromHTML,
} from "draft-js";
import draftToHtml from "draftjs-to-html";
import styledTextEditor from "./TextEditor.css";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";

export const TextEditorCreate = ({ setBlogContent, blogContent }) => {
  const [editorState, setEditorState] = useState();

  useEffect(() => {
    setEditorState(EditorState.createEmpty());
  }, []);

  useEffect(() => {
    if (editorState) {
      setBlogContent(
        draftToHtml(convertToRaw(editorState.getCurrentContent()))
      );
    }
  }, [editorState]);

  return (
    <div>
      <Editor
        editorState={editorState}
        toolbarClassName="toolbarClassName"
        wrapperClassName="wrapperClassName"
        editorClassName="editorClassName"
        onEditorStateChange={setEditorState}
      />
    </div>
  );
};

export const TextEditorUpdate = ({ setBlogContent, blogContent }) => {
  const [editorState, setEditorState] = useState();
  const [repeatUseEffect, setrepeatUseEffect] = useState(false);

  useEffect(() => {
    if (blogContent) {
      setEditorState(
        EditorState.createWithContent(
          ContentState.createFromBlockArray(convertFromHTML(blogContent))
        )
      );
    } else {
      setrepeatUseEffect(!repeatUseEffect);
    }
  }, [repeatUseEffect]);

  useEffect(() => {
    if (editorState) {
      setBlogContent(
        draftToHtml(convertToRaw(editorState.getCurrentContent()))
      );
    }
  }, [editorState]);

  return (
    <div>
      <Editor
        editorState={editorState}
        toolbarClassName="toolbarClassName"
        wrapperClassName="wrapperClassName"
        editorClassName="editorClassName"
        onEditorStateChange={setEditorState}
      />
    </div>
  );
};
