import { useState, useEffect } from "react";
import Blog from "../Blog/Blog.js";
import styledBlogs from "./Blogs.css";
import axios from "axios";
import BlogCategories from "../BlogCategories/BlogCategories.js";
import io from "socket.io-client";

const socket = io.connect("https://ibaw-blog-backend.herokuapp.com/");

const Blogs = ({ setCreatedBlogId }) => {
  const [currentCategroy, setCurrentCategroy] = useState("All");
  const [blogs, setBlogs] = useState([]);

  useEffect(() => {
    socket.on("message", ({ blogId }) => {
      setCreatedBlogId(blogId);
    });
  });

  useEffect(() => {
    try {
      const url = `https://ibaw-blog-backend.herokuapp.com/blog`;
      axios
        .get(url)
        .then((res) => {
          setBlogs(res.data);
        })
        .catch();
    } catch (err) {
      console.log(err);
    }
  }, []);

  const clickToBlog = (currentBlog) => {
    setBlogs(
      blogs.filter((blog) => {
        return blog._id == currentBlog._id;
      })
    );
  };
  return (
    <>
      <div className="login-forms">
        <BlogCategories
          currentCategroy={currentCategroy}
          setCurrentCategroy={setCurrentCategroy}
        />
      </div>

      <section className={blogs.length > 1 ? "grid-blogs" : ""}>
        {currentCategroy === "All"
          ? blogs.map((blog) => (
              <Blog
                key={blog._id}
                blogs={blogs}
                blog={blog}
                onClick={clickToBlog}
              />
            ))
          : blogs
              .filter((blog) => blog.category == currentCategroy)
              .map((blog) => (
                <Blog
                  key={blog._id}
                  blogs={blogs}
                  blog={blog}
                  onClick={clickToBlog}
                />
              ))}
      </section>
    </>
  );
};

export default Blogs;
