import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";

import styledBlog from "./Blog.css";

const Blog = ({ blogs, blog, onClick }) => {
  const severalEntries = blogs.length > 1;
  const [category, setCategory] = useState("");
  const [blogEntryClicked, setBlogEntryClicked] = useState(false);

  useEffect(() => {
    setCategory(blog.category);
  }, []);

  return (
    <>
      {blogEntryClicked ? (
        ""
      ) : (
        <Link
          to={{
            pathname: "/BlogEntry",
            state: {
              blog: blog,
            },
          }}
          blog={"test"}
        >
          <div
            className={severalEntries ? "blogEntries" : "blogEntry"}
            href="/BlogEntry"
          >
            {category === "Soccer" ? <div className="soccer image"></div> : ""}
            {category === "Running" ? (
              <div className="running image"></div>
            ) : (
              ""
            )}
            {category === "Cycling" ? (
              <div className="cycling image"></div>
            ) : (
              ""
            )}
            {category === "Hockey" ? <div className="hockey image"></div> : ""}

            <p className="category">{blog.category}</p>
            {category === "Soccer" ? (
              <i className="small-icons far fa-futbol"></i>
            ) : (
              ""
            )}
            {category === "Running" ? (
              <i className="small-icons fas fa-running"></i>
            ) : (
              ""
            )}
            {category === "Cycling" ? (
              <i className="small-icons fas fa-biking"></i>
            ) : (
              ""
            )}
            {category === "Hockey" ? (
              <i className="small-icons fas fa-hockey-puck"></i>
            ) : (
              ""
            )}

            <h3>{blog.title}</h3>
          </div>
        </Link>
      )}
    </>
  );
};

export default Blog;
