import React, { useState, useEffect } from "react";
import styledSignUp from "./SignUp.css";
import Validation from "../Validation/Validation";
import Success from "../Success/Success";
import axios from "axios";

const SignUp = ({ submitForm }) => {
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [confirmedPassword, setConfirmedPassword] = useState("");
  const [createBlog, setCreateBlog] = useState(false);
  const [errors, setErrors] = useState({});
  const [dataIsCorrect, setDataIsCorrect] = useState(false);
  const [successfulMessage, setSuccessfulMessage] = useState("");
  const [errorMessage, setErrorMessage] = useState("");
  const [proceedWithCreation, setProceedWithCreation] = useState(false);

  const handlerForSubmit = async (e) => {
    e.preventDefault();
    setErrorMessage("");
    setSuccessfulMessage("");
    setErrors(Validation({ username, email, password, confirmedPassword }));
    setDataIsCorrect(true);
  };

  const createUser = (user) => {
    try {
      const url = `https://ibaw-blog-backend.herokuapp.com/userAccount/`;
      const newUser = {
        username: user.username,
        email: user.email,
        password: user.password,
      };
      axios.post(url, newUser).then((res) => {
        if (res.data.success) {
          setSuccessfulMessage(res.data.message);
        } else {
          setErrorMessage(res.data.message);
        }
      });
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    if (Object.keys(errors).length === 0 && dataIsCorrect) {
      createUser({ username, email, password });
      //submitForm(true);
    }
  }, [errors]);

  return (
    <>
      {successfulMessage ? (
        <Success text={successfulMessage} />
      ) : (
        <div className="flex-wrapper">
          {!createBlog ? (
            <div className="flex-login-form">
              <div className="login-box">
                <div className="imgcontainer">
                  <img
                    src="https://media.geeksforgeeks.org/wp-content/cdn-uploads/20190710102234/download3.png"
                    alt="Avatar"
                    className="avatar"
                  />
                </div>
                <div className="login-forms">
                  <div className="input-div username">
                    <div className="i">
                      <i className="fas fa-user"></i>
                    </div>
                    <div className="col-3">
                      <label className="login-labels">Username:</label>
                      <input
                        className="effect-1"
                        type="Text"
                        placeholder="Add your username"
                        value={username}
                        onChange={(e) => setUsername(e.target.value)}
                      />
                      <span className="focus-border"></span>
                    </div>
                    <div className="placeholder"></div>
                    {errors.username && (
                      <p className="error">{errors.username}</p>
                    )}
                  </div>
                </div>

                <div className="login-forms">
                  <div className="input-div email">
                    <div className="i">
                      <i className="fas fa-envelope"></i>
                    </div>
                    <div className="col-3">
                      <label className="login-labels">E-Mail:</label>
                      <input
                        className="effect-1"
                        type="Text"
                        placeholder="Add your E-Mail"
                        value={email}
                        onChange={(e) => setEmail(e.target.value)}
                      />
                      <span className="focus-border"></span>
                    </div>
                    <div className="placeholder"></div>
                    {errors.email && <p className="error">{errors.email}</p>}
                  </div>
                </div>

                <div className="login-forms">
                  <div className="input-div password">
                    <div className="i">
                      <i className="fas fa-lock"></i>
                    </div>
                    <div className="col-3">
                      <label className="login-labels">Passwort:</label>
                      <input
                        className="effect-1"
                        type="password"
                        placeholder="Add your password"
                        value={password}
                        onChange={(e) => setPassword(e.target.value)}
                      />
                      <span className="focus-border"></span>
                    </div>
                    <div className="placeholder"></div>
                    {errors.password && (
                      <p className="error">{errors.password}</p>
                    )}
                  </div>
                </div>
                <div className="login-forms">
                  <div className="input-div password">
                    <div className="i">
                      <i className="fas fa-lock"></i>
                    </div>
                    <div className="col-3">
                      <label className="login-labels">Confirm Passwort:</label>
                      <input
                        className="effect-1"
                        type="password"
                        placeholder="Confirm password"
                        value={confirmedPassword}
                        onChange={(e) => setConfirmedPassword(e.target.value)}
                      />
                      <span className="focus-border"></span>
                    </div>
                    <div className="placeholder"></div>
                    {errors.confirmedPassword && (
                      <p className="error">{errors.confirmedPassword}</p>
                    )}
                  </div>
                </div>

                <div className="login-forms">
                  <button onClick={handlerForSubmit}>Sign Up</button>
                </div>
                {errorMessage && <p className="error">{errorMessage}</p>}
              </div>
            </div>
          ) : (
            ""
          )}
        </div>
      )}
    </>
  );
};

export default SignUp;
