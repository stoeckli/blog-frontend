import React, { useState } from "react";
import styledUploadProfileImage from "./UploadProfileImage.css";
import ProgressBar from "../ProgressBar/ProgressBar.js";

const UploadProfileImage = ({ uploadedURL, setUploadedURL }) => {
  const [file, setFile] = useState(null);
  const [error, setError] = useState(null);
  const [uploadedFileName, setUploadedFileName] = useState(null);

  const types = ["image/png", "image/jpg", "image/jpeg"];

  const imageHandler = (e) => {
    let selected = e.target.files[0];
    if (selected && types.includes(selected.type)) {
      setFile(selected);
      setError("");
    } else {
      setFile(null);
      setError("Please select an image file (png or jpeg)");
    }
  };

  return (
    <>
      <div className="img-holder">
        <img
          src={uploadedURL}
          alt="Profile"
          id="img"
          className="profile-img"
        ></img>
      </div>
      <input
        type="file"
        className="image-selector"
        name="image-upload"
        id="upload-img"
        accept="image/*"
        onChange={imageHandler}
      ></input>
      <div className="label">
        <label htmlFor="upload-img" className="image-upload">
          Choose image
        </label>
      </div>
      <div className="output">
        {error && <div className="error">{error}</div>}
        {file && <div className="fileName">{file.name}</div>}
        {file && (
          <ProgressBar
            file={file}
            setFile={setFile}
            setUploadedURL={setUploadedURL}
            setUploadedFileName={setUploadedFileName}
            foldername="Profil images"
          />
        )}
      </div>
    </>
  );
};

export default UploadProfileImage;
