import { useState, useEffect } from "react";
import { projectStorage } from "../firebase/config.js";
import { getDownloadURL, uploadBytesResumable } from "@firebase/storage";
import { ref } from "@firebase/storage";

const useStorage = (file, foldername) => {
  const [progress, setProgress] = useState(0);
  const [error, setError] = useState(null);
  const [url, setUrl] = useState(null);

  useEffect(() => {
    const storageRef = ref(projectStorage, `/${foldername}/${file.name}`);
    const uploadTask = uploadBytesResumable(storageRef, file);

    uploadTask.on(
      "state_changed",
      (snapshot) => {
        const prog = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;

        setProgress(prog);
      },
      (err) => setError(err),
      () => {
        getDownloadURL(uploadTask.snapshot.ref).then((url) => setUrl(url));
      }
    );
  }, [file]);
  return { progress, url, error };
};

export default useStorage;
