import { projectStorage } from "../firebase/config.js";
import { deleteObject } from "@firebase/storage";
import { ref } from "@firebase/storage";

export default function deleteStorage(uploadedFileName) {
  const storageRef = ref(projectStorage, `/files/${uploadedFileName}`);
  deleteObject(storageRef)
    .then(() => {
      console.log("file deletes succesfully");
    })
    .catch((err) => {
      console.log(err);
    });
}
