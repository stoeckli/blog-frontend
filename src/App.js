import Footer from "./components/Footer/Footer.js";
import Blogs from "./components/Blogs/Blogs.js";
import Navigation from "./components/Navigation/Navigation.js";
import Login from "./components/Login/Login.js";
import SignUp from "./components/SignUp/SignUp.js";
import Home from "./components/Home/Home.js";
import Profile from "./components/Profile/Profile.js";
import { useState, useEffect } from "react";
import axios from "axios";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import CreateBlogEntry from "./components/CreateBlogEntry/CreateBlogEntry.js";
import ForgotPassword from "./components/ForgotPassword/ForgotPassword.js";
import PersonalBlogEntries from "./components/PersonalBlogEntries/PersonalBlogEntries.js";
import { ResetPassword } from "./components/ResetPassword/ResetPassword.js";
import BlogEntry from "./components/BlogEntry/BlogEntry.js";
import UpdateBlogEntry from "./components/UpdateBlogEntry/UpdateBlogEntry.js";
import Success from "./components/Success/Success.js";
import Cookies from "js-cookie";
import Socket from "./components/Socket/Socket.js";

import io from "socket.io-client";

const socket = io.connect("https://ibaw-blog-backend.herokuapp.com/");

const App = () => {
  const [userLogged, setUserLogged] = useState(false);
  const [loginMask, setLoginMask] = useState(false);
  const [token, setToken] = useState("");
  const [loggedUserName, setloggedUserName] = useState("");
  const [createdBlogId, setCreatedBlogId] = useState("");
  const [profileImageURL, setProfileImageURL] = useState("");

  useEffect(() => {
    socket.on("message", ({ blogId }) => {
      setCreatedBlogId(blogId);
    });
  });

  useEffect(() => {
    try {
      const currentToken = Cookies.get("token");
      if (currentToken) {
        setloggedUserName(Cookies.get("username"));
        const url = `https://ibaw-blog-backend.herokuapp.com/userAccount/verify`;

        axios
          .get(url, { headers: { Authorization: currentToken } })
          .then((res) => {
            if (res.data.success) {
              setToken(currentToken);
              setUserLogged(true);
            } else {
              console.log("no valid token");
              setUserLogged(false);
            }
          });
      } else {
        console.log("no token found");
      }
    } catch (err) {
      console.log("no valid token");
    }
  }, []);

  useEffect(() => {
    try {
      const userInformation = {
        username: Cookies.get("username"),
      };
      if (userInformation) {
        const url = `https://ibaw-blog-backend.herokuapp.com/userAccount/imageUrl`;
        const currentToken = Cookies.get("token");
        axios
          .post(
            url,
            userInformation,
            { headers: { Authorization: currentToken } }
            //{ withCredentials: true }
          )
          .then((res) => {
            if (res.data.success) {
              if (res.data.imageUrl) {
                setProfileImageURL(res.data.imageUrl);
              }
            }
          });
      }
    } catch (err) {
      console.log(err);
    }
  }, [userLogged]);

  return (
    <BrowserRouter>
      <div id="wrapper">
        <Navigation
          loginMask={loginMask}
          setLoginMask={setLoginMask}
          token={token}
          setToken={setToken}
          loggedUserName={loggedUserName}
          setUserLogged={setUserLogged}
          userLogged={userLogged}
          profileImageURL={profileImageURL}
        />
        <div className="container">
          <Switch>
            <Route exact path="/" component={Home} />
            <Route exact path="/signup" component={SignUp} />
            <Route exact path="/login">
              <Login userLogged={userLogged} setUserLogged={setUserLogged} />
            </Route>
            <Route exact path="/createblog">
              <CreateBlogEntry setCreatedBlogId={setCreatedBlogId} />
            </Route>

            <Route exact path="/forgot" component={ForgotPassword} />
            <Route exact path="/reset/:id" component={ResetPassword} />
            <Route exact path="/blogs" component={Blogs}>
              <Blogs setCreatedBlogId={setCreatedBlogId} />
            </Route>
            <Route exact path="/blogEntry" component={BlogEntry} />
            <Route exact path="/updateBlogEntry" component={UpdateBlogEntry} />
            <Route path="/profile">
              <Profile userLogged={userLogged} setUserLogged={setUserLogged} />
            </Route>
            <Route exact path="/success" component={Success} />
            <Route exact path="/socket" component={Socket} />
            <Route exact path="/socket">
              <Socket createdBlogId={createdBlogId} />
            </Route>
            <Route exact path="/myBlogs" component={PersonalBlogEntries} />
          </Switch>
        </div>
        {createdBlogId !== "" ? <Socket createdBlogId={createdBlogId} /> : ""}
        <Footer />
      </div>
    </BrowserRouter>
  );
};

export default App;
