import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore";
import { getStorage } from "firebase/storage";

const firebaseConfig = {
  apiKey: "AIzaSyB472pdHp3HvicSJtGyRgh3TBDA8SiaKU0",
  authDomain: "blog-firebase-87efd.firebaseapp.com",
  projectId: "blog-firebase-87efd",
  storageBucket: "blog-firebase-87efd.appspot.com",
  messagingSenderId: "506153985718",
  appId: "1:506153985718:web:2c35aae4a12be487046817",
};

export const app = initializeApp(firebaseConfig);

const projectStorage = getStorage(app);
const projectFirestore = getFirestore();

export { projectStorage, projectFirestore };
